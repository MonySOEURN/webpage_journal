<?php

use App\Mail\SubmitArticleMail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SearchJournalController;
use PhpParser\Node\Expr\FuncCall;
use App\Http\Controllers\MailController;
use App\Http\Controllers\SubmitJournalController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('guest-pages.index');
});
Route::get('/aim_and_scope', function () {
    return view('guest-pages.aim_and_scope');
});
Route::get('/instruction', function () {
    return view('guest-pages.instruction');
});
Route::get('/issue', function () {
    return view('guest-pages.issue');
});
Route::get('/board', function () {
    return view('guest-pages.editorial_board');
});
Route::get('/editorial_board_member', function () {
    return view('guest-pages.editorial_board_member');
});
Route::get('/policy', function () {
    return view('guest-pages.policy');
});

Route::get('/current-issues', function () {
    return view('guest-pages.current_issues');
});

Route::get('/browse-issues', function () {
    return view('guest-pages.browse_issues');
});

// Route::get('/email', function(){
//     return view('guest-pages.submitedFormpdf');
    // Mail::to('email@email.com')->send(new SubmitArticleMail());
    // return new SubmitArticleMail();
// });
Route::get('/email', [MailController::class, 'sendMail']);
Route::get('/search', [SearchJournalController::class, 'getResult']);

Route::get('/email', function(){
    return view('guest-pages.submitedFormpdf');
// Route::get('/email', function(){
//     return view('guest-pages.submitedFormpdf');
    // Mail::to('email@email.com')->send(new SubmitArticleMail());
    // return new SubmitArticleMail();
});
Route::get('/email', [MailController::class, 'sendMail']);

// Route::group(['prefix'=>'volume-1'], function(){
Route::get('/issue-1-number-1', function () {
    return view('volumes.1.issue-1-number-1');
});
Route::get('/issue-1-number-2', function () {
    return view('volumes.1.issue-1-number-2');
});
Route::get('/issue-2-number-1', function () {
    return view('volumes.1.issue-2-number-1');
});
// });

Route::get('/submit_journal', [SubmitJournalController::class, 'submitJournalForm']);

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');
