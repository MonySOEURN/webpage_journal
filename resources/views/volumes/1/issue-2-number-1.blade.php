
@extends('app')

@section('content')

    <style>

    .card-subtitle2{
        font-size: 14px;
    }
    .issue-title{
        font-weight: 10px;
    }
    .card:hover{
        cursor: pointer;
        transform: scale(1.001);
        box-shadow: 0 10px 20px rgba(0,0,0,.12), 0 4px 8px rgba(0,0,0,.06);
    }

    </style>

    {{-- <h2>issue-1-number-1</h2> --}}
    <div class="col-lg-8 bg-light"><br>
        <h2>Cambodia Journal of Basic and Applied Research (CJBAR), Volume 2, Issue 1 Number-1 (2020)</h2>
        <div class="mb-5 mt-3">
            
            <h5 class="issue-title ">Editorial:</h5>
            <div class="accordion" id="accordionExample">
                
                <div class="card mt-3">
                    <div class="card-body">
                        <h6 class="card-subtitle text-muted">Editorial</h6>
                        <h5 class="card-title">Dangers and opportunities related to the COVID-19 pandemic for Higher Education Institutions in Cambodia </h5>
                        <h6 class="card-subtitle2 mb-2 text-muted">CHET Chealy, and SOK Serey</h6>
                        <h6 class="card-subtitle2 mb-2 text-muted">pages: 20–26</h6>
                        <h6 class="card-subtitle2 mb-2 text-muted"><strong> Published online:</strong> 30 June 2020</h6>
                        {{-- <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p> --}}
                        <a href="/files/CJABJ-Issue-2-Number-1/CJABJ-Issue-2-Number-1_paper 1.pdf" class="card-link" download>PDF download</a>
                      </div>
                    
                </div>
            </div>

            <h5 class="issue-title mt-4">Research Article:</h5>
            <div class="accordion" id="accordionExample">
                <div class="card mt-3">
                    <div class="card-body">
                        <h6 class="card-subtitle text-muted">Article</h6>
                        <h5 class="card-title">The integration of renewable energy sources and IoT devices as a future sustainable energy solution</h5>
                        <h6 class="card-subtitle2 mb-2 text-muted">TY Bunly, and CHEY Chan Oeurn</h6>
                        <h6 class="card-subtitle2 mb-2 text-muted">pages: 27–40</h6>
                        <h6 class="card-subtitle2 mb-2 text-muted"><strong> Published online:</strong> 30 June 2020</h6>
                        <a href="/files/CJABJ-Issue-2-Number-1/CJABJ-Issue-2-Number-1_paper 2.pdf" class="card-link" download>PDF download</a>
                      </div>
                    
                </div>
            </div>
            <div class="accordion" id="accordionExample">
                <div class="card mt-3">
                    <div class="card-body">
                        <h6 class="card-subtitle text-muted">Article</h6>
                        <h5 class="card-title">Using linear programming to optimize rice yields in Battambang, Cambodia </h5>
                        <h6 class="card-subtitle2 mb-2 text-muted">VENG Sotheara, CHAU Cheuchhoeng, and YONN Odom</h6>
                        <h6 class="card-subtitle2 mb-2 text-muted">pages: 41–56</h6>
                        <h6 class="card-subtitle2 mb-2 text-muted"><strong> Published online:</strong> 30 June 2020</h6>
                        <a href="/files/CJABJ-Issue-2-Number-1/CJABJ-Issue-2-Number-1_paper 3.pdf" class="card-link" download>PDF download</a>
                      </div>
                    
                </div>
            </div>
            <div class="accordion" id="accordionExample">
                <div class="card mt-3">
                    <div class="card-body">
                        <h6 class="card-subtitle text-muted">Article</h6>
                        <h5 class="card-title">Seasonal poverty in Zimbabwe and Cambodia: A comparative analysis of the developing world </h5>
                        <h6 class="card-subtitle2 mb-2 text-muted">BLESSING Gweshengwe</h6>
                        <h6 class="card-subtitle2 mb-2 text-muted">pages: 57–88</h6>
                        <h6 class="card-subtitle2 mb-2 text-muted"><strong> Published online:</strong> 30 June 2020</h6>
                        <a href="/files/CJABJ-Issue-2-Number-1/CJABJ-Issue-2-Number-1_paper 4.pdf" class="card-link" download>PDF download</a>
                      </div>
                    
                </div>
            </div>
            <div class="accordion" id="accordionExample">
                <div class="card mt-3">
                    <div class="card-body">
                        <h6 class="card-subtitle text-muted">Article</h6>
                        <h5 class="card-title">Sources of income in rural households at Kdol Tahaen Commune, Bavel District, Battambang </h5>
                        <h6 class="card-subtitle2 mb-2 text-muted">THATH Rido, and RATH Sethik</h6>
                        <h6 class="card-subtitle2 mb-2 text-muted">pages: 89–118</h6>
                        <h6 class="card-subtitle2 mb-2 text-muted"><strong> Published online:</strong> 30 June 2020</h6>
                        <a href="/files/CJABJ-Issue-2-Number-1/CJABJ-Issue-2-Number-1_paper 5.pdf" class="card-link" download>PDF download</a>
                      </div>
                    
                </div>
            </div>
            <div class="accordion" id="accordionExample">
                <div class="card mt-3">
                    <div class="card-body">
                        <h6 class="card-subtitle text-muted">Article</h6>
                        <h5 class="card-title">Higher education in Cambodia: Engendered, postcolonial Western influences and Asian values</h5>
                        <h6 class="card-subtitle2 mb-2 text-muted">IM Keun</h6>
                        <h6 class="card-subtitle2 mb-2 text-muted">pages: 119–151</h6>
                        <h6 class="card-subtitle2 mb-2 text-muted"><strong> Published online:</strong> 30 June 2020</h6>
                        <a href="/files/CJABJ-Issue-2-Number-1/CJABJ-Issue-2-Number-1_paper 6.pdf" class="card-link" download>PDF download</a>
                      </div>
                    
                </div>
            </div>
            <div class="accordion" id="accordionExample">
                <div class="card mt-3">
                    <div class="card-body">
                        <h6 class="card-subtitle text-muted">Article</h6>
                        <h5 class="card-title">Capacity building of secondary school principals: A case study of the School Leadership Upgrading Program (2019-2020) at the Royal University of Phnom Penh (RUPP) </h5>
                        <h6 class="card-subtitle2 mb-2 text-muted">SOK Soth, MAM Socheath, and KEO Sarath</h6>
                        <h6 class="card-subtitle2 mb-2 text-muted">pages: 152–172</h6>
                        <h6 class="card-subtitle2 mb-2 text-muted"><strong> Published online:</strong> 30 June 2020</h6>
                        <a href="/files/CJABJ-Issue-2-Number-1/CJABJ-Issue-2-Number-1_paper 7.pdf" class="card-link" download>PDF download</a>
                      </div>
                    
                </div>
            </div>

            <h5 class="issue-title mt-4">Book Reviews:</h5>
            <div class="accordion" id="accordionExample">
                <div class="card mt-3">
                    <div class="card-body">
                        <h6 class="card-subtitle text-muted">Article</h6>
                        <h5 class="card-title">Urban development in the margins of a world heritage site: In the shadows of Angkor. By Adèle Esposito. Amsterdam: Amsterdam University Press, 2018. 337 pp. Price: €105 (Hardcover) and €104.99 (eBook PDF). </h5>
                        <h6 class="card-subtitle2 mb-2 text-muted">THUON Try</h6>
                        <h6 class="card-subtitle2 mb-2 text-muted">pages: 173–183</h6>
                        <h6 class="card-subtitle2 mb-2 text-muted"><strong> Published online:</strong> 30 June 2020</h6>
                        <a href="/files/CJABJ-Issue-2-Number-1/CJABJ-Issue-2-Number-1_paper 8.pdf" class="card-link" download>PDF download</a>
                      </div>
                    
                </div>
            </div>
        </div>
    </div>

@endsection