
@extends('app')

@section('content')

    <style>
    .issue-title{
        font-weight: 10px;
    }
    .card:hover{
        cursor: pointer;
        transform: scale(1.001);
        box-shadow: 0 10px 20px rgba(0,0,0,.12), 0 4px 8px rgba(0,0,0,.06);
    }

    </style>

    {{-- <h2>issue-1-number-1</h2> --}}
    <div class="col-lg-8 bg-light"><br>
        <h2>Cambodia Journal of Basic and Applied Research (CJBAR), Volume 1, Issue 1 Number-2 (2019)</h2>
        <div class="mb-5 mt-3">
            <h5 class="issue-title ">Editorial:</h5>
            <div class="accordion" id="accordionExample">
                
                <div class="card mt-3">
                    <div class="card-body">
                        <h6 class="card-subtitle text-muted">Editorial</h6>
                        <h5 class="card-title">Promotion of Research Culture at Higher Education Institutions (HEIs) in Cambodia </h5>
                        <h6 class="card-subtitle2 mb-2 text-muted">CHET Chealy</h6>
                        <h6 class="card-subtitle2 mb-2 text-muted">pages: 05–08</h6>
                        <h6 class="card-subtitle2 mb-2 text-muted"><strong> Published online:</strong> 31 December 2019</h6>
                        <a href="/files/CJBA-Vol-1-Issue-2-2019/CJBA-Vol-1-Issue-2-2019_ paper 1.pdf" class="card-link" download>PDF download</a>
                      </div>
                    
                </div>
            </div>

            <h5 class="issue-title mt-4">Research Article:</h5>
            <div class="accordion" id="accordionExample">
                <div class="card mt-3">
                    <div class="card-body">
                        <h6 class="card-subtitle text-muted">Article</h6>
                        <h5 class="card-title">Local Community Engagement as a Pathway toward Sustainable Development through Higher Education Institutions (HEIs) in Cambodia </h5>
                        <h6 class="card-subtitle2 mb-2 text-muted">MAM Socheath, KOVIN Chuenchanok & SINTHUNAWA Chirapol</h6>
                        <h6 class="card-subtitle2 mb-2 text-muted">pages: 09–32</h6>
                        <h6 class="card-subtitle2 mb-2 text-muted"><strong> Published online:</strong> 31 December 2019</h6>
                        <a href="/files/CJBA-Vol-1-Issue-2-2019/CJBA-Vol-1-Issue-2-2019_paper 2.pdf" class="card-link" download>PDF download</a>
                    </div>
                    
                </div>
            </div>
            <div class="accordion" id="accordionExample">
                <div class="card mt-3">
                    <div class="card-body">
                        <h6 class="card-subtitle text-muted">Article</h6>
                        <h5 class="card-title">Log and bag cultivation of Oyster (Pleurotus ostreatus) and Lingzhi (Ganoderma lucidum) mushrooms in Cambodia </h5>
                        <h6 class="card-subtitle2 mb-2 text-muted">SREY Chanshorphea</h6>
                        <h6 class="card-subtitle2 mb-2 text-muted">pages: 33–43</h6>
                        <h6 class="card-subtitle2 mb-2 text-muted"><strong> Published online:</strong> 31 December 2019</h6>
                        <a href="/files/CJBA-Vol-1-Issue-2-2019/CJBA-Vol-1-Issue-2-2019_paper 3.pdf" class="card-link" download>PDF download</a>
                    </div>
                    
                </div>
            </div>
            <div class="accordion" id="accordionExample">
                <div class="card mt-3">
                    <div class="card-body">
                        <h6 class="card-subtitle text-muted">Article</h6>
                        <h5 class="card-title">Gender Inequality within Higher Education Institutions: a case study of the Royal University of Phnom Penh</h5>
                        <h6 class="card-subtitle2 mb-2 text-muted">DOUNG Chandy</h6>
                        <h6 class="card-subtitle2 mb-2 text-muted">pages: 44–62</h6>
                        <h6 class="card-subtitle2 mb-2 text-muted"><strong> Published online:</strong> 31 December 2019</h6>
                        <a href="/files/CJBA-Vol-1-Issue-2-2019/CJBA-Vol-1-Issue-2-2019_paper 4.pdf" class="card-link" download>PDF download</a>
                    </div>
                    
                </div>
            </div>
            <div class="accordion" id="accordionExample">
                <div class="card mt-3">
                    <div class="card-body">
                        <h6 class="card-subtitle text-muted">Article</h6>
                        <h5 class="card-title">Integration of Social-Aedia-Apps into Second Language Learning Activities to Improve Critical Thinking in Chinese Foreign Language Learning </h5>
                        <h6 class="card-subtitle2 mb-2 text-muted">THAN Chhorn</h6>
                        <h6 class="card-subtitle2 mb-2 text-muted">pages: 63–83</h6>
                        <h6 class="card-subtitle2 mb-2 text-muted"><strong> Published online:</strong> 31 December 2019</h6>
                        <a href="/files/CJBA-Vol-1-Issue-2-2019/CJBA-Vol-1-Issue-2-2019_paper 5.pdf" class="card-link" download>PDF download</a>
                    </div>
                    
                </div>
            </div>
            <div class="accordion" id="accordionExample">
                <div class="card mt-3">
                    <div class="card-body">
                        <h6 class="card-subtitle text-muted">Article</h6>
                        <h5 class="card-title">The impact of flooding on primary education in Kroch Chmar District, Tbong Khmum Province, Cambodia</h5>
                        <h6 class="card-subtitle2 mb-2 text-muted">HENG Chanla</h6>
                        <h6 class="card-subtitle2 mb-2 text-muted">pages: 84–98</h6>
                        <h6 class="card-subtitle2 mb-2 text-muted"><strong> Published online:</strong> 31 December 2019</h6>
                        <a href="/files/CJBA-Vol-1-Issue-2-2019/CJBA-Vol-1-Issue-2-2019_paper 6.pdf" class="card-link" download>PDF download</a>
                    </div>
                    
                </div>
            </div>
            <div class="accordion" id="accordionExample">
                <div class="card mt-3">
                    <div class="card-body">
                        <h6 class="card-subtitle text-muted">Article</h6>
                        <h5 class="card-title">A Decade of Communal Land Titling in Cambodia: Achievements and Ways Forward </h5>
                        <h6 class="card-subtitle2 mb-2 text-muted">THOL Dina1 & OEUR Il</h6>
                        <h6 class="card-subtitle2 mb-2 text-muted">pages: 99–107</h6>
                        <h6 class="card-subtitle2 mb-2 text-muted"><strong> Published online:</strong> 31 December 2019</h6>
                        <a href="/files/CJBA-Vol-1-Issue-2-2019/CJBA-Vol-1-Issue-2-2019_paper 7.pdf" class="card-link" download>PDF download</a>
                    </div>
                    
                </div>
            </div>

            <h5 class="issue-title mt-4">Book Review:</h5>
            <div class="accordion" id="accordionExample">
                <div class="card mt-3">
                    <div class="card-body">
                        <h5 class="card-title">The Discourse of Peer Review: Reviewing Submissions to Academic Journals, Brian Paltridge. Palgrave Macmillan, London (2017). xiv + 235 pp. ISBN 978-1-137-48735-3. ISBN 978-1-137-48736-0 (eBook). Hardcover €99.99. E-book €83.29. </h5>
                        <h6 class="card-subtitle2 mb-2 text-muted">HENG Kimkong</h6>
                        <h6 class="card-subtitle2 mb-2 text-muted">pages: 108–114</h6>
                        <h6 class="card-subtitle2 mb-2 text-muted"><strong> Published online:</strong> 31 December 2019</h6>
                        <a href="/files/CJBA-Vol-1-Issue-2-2019/CJBA-Vol-1-Issue-2-2019_paper 8.pdf" class="card-link" download>PDF download</a>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>

@endsection