
@extends('app')

@section('content')

    <style>
    .card-subtitle2{
        font-size: 14px;
    }
    .issue-title{
        font-weight: 10px;
    }
    .card:hover{
        cursor: pointer;
        transform: scale(1.001);
        box-shadow: 0 10px 20px rgba(0,0,0,.12), 0 4px 8px rgba(0,0,0,.06);
    }

    </style>

    {{-- <h2>issue-1-number-1</h2> --}}
    <div class="col-lg-8 bg-light"><br>
        <h2>Cambodia Journal of Basic and Applied Research (CJBAR), Volume 1, Issue 1 Number-1 (2019)</h2>
        <div class="mb-5 mt-3">
            <h5 class="issue-title ">Editorial:</h5>
            <div class="accordion" id="accordionExample">
                
                <div class="card mt-3">
                    <div class="card-body">
                        <h6 class="card-subtitle text-muted">Editorial</h6>
                        <h5 class="card-title">The internationalization of higher education
                            in Cambodia</h5>
                        <h6 class="card-subtitle2 mb-2 text-muted">CHET Chealy & UN Leang</h6>
                        <h6 class="card-subtitle2 mb-2 text-muted">pages: 06 – 15</h6>
                        <h6 class="card-subtitle2 mb-2 text-muted"><strong> Published online:</strong> 30 June 2019</h6>
                        {{-- <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p> --}}
                        <a href="/files/CJABJ-Issue-1-Number-1/CJABJ-Issue-1-Number-1_ paper 1.pdf" class="card-link" download>PDF download</a>
                      </div>
                    
                </div>
            </div>

            <h5 class="issue-title mt-4">Research Article:</h5>
            <div class="accordion" id="accordionExample">
                
                <div class="card mt-3">
                    <div class="card-body">
                        <h6 class="card-subtitle text-muted">Article</h6>
                        <h5 class="card-title">Preliminary research on insect diversity at Kulen Promtep Wildlife Sanctuary, Cambodia</h5>
                        <h6 class="card-subtitle2 mb-2 text-muted">PHAUK Sophany*, RIM Socheata, KEATH Sophorn, KEUM Theary, DOEURK Bros & HOT Cheysen</h6>
                        <h6 class="card-subtitle2 mb-2 text-muted">pages: 16–48</h6>
                        <h6 class="card-subtitle2 mb-2 text-muted"><strong> Published online:</strong> 30 June 2019</h6>
                        <a href="/files/CJABJ-Issue-1-Number-1/CJABJ-Issue-1-Number-1_paper 2.pdf" class="card-link" download>PDF download</a>
                      </div>
                    
                </div>
            </div>
            <div class="accordion" id="accordionExample">
                
                <div class="card mt-3">
                    <div class="card-body">
                        <h6 class="card-subtitle text-muted">Article</h6>
                        <h5 class="card-title">Simulating future trends in stormwater runoff at a university campus related to climate and land use changes</h5>
                        <h6 class="card-subtitle2 mb-2 text-muted">SOVANN C. Pheaktra, CHOI Daniel, THOU Sothean, YIM Savuth</h6>
                        <h6 class="card-subtitle2 mb-2 text-muted">pages: 49–75</h6>
                        <h6 class="card-subtitle2 mb-2 text-muted"><strong> Published online:</strong> 30 June 2019</h6>
                        <a href="/files/CJABJ-Issue-1-Number-1/CJABJ-Issue-1-Number-1_paper 3.pdf" class="card-link" download>PDF download</a>
                      </div>
                    
                </div>
            </div>
            <div class="accordion" id="accordionExample">
                
                <div class="card mt-3">
                    <div class="card-body">
                        <h6 class="card-subtitle text-muted">Article</h6>
                        <h5 class="card-title">Prevalence and concentration of Escherichia coli and Salmonella species in fresh vegetables collected from different types of markets in Phnom Penh</h5>
                        <h6 class="card-subtitle2 mb-2 text-muted">PHOEURK Chanrith, TIENG Siteng, TAN Sreypich, MOEUNG Sreylak, CHEU Seanghong, CHEAN Pich Raksmey, HAY Vannith, SAY Channara, LIM Lyheng & KANN Leakhena</h6>
                        <h6 class="card-subtitle2 mb-2 text-muted">pages: 76–96</h6>
                        <h6 class="card-subtitle2 mb-2 text-muted"><strong> Published online:</strong> 30 June 2019</h6>
                        <a href="/files/CJABJ-Issue-1-Number-1/CJABJ-Issue-1-Number-1_paper 4.pdf" class="card-link" download>PDF download</a>
                      </div>
                    
                </div>
            </div>
            <div class="accordion" id="accordionExample">
                
                <div class="card mt-3">
                    <div class="card-body">
                        <h6 class="card-subtitle text-muted">Article</h6>
                        <h5 class="card-title">Seasonal variation in the nitrate concentration of groundwater samples surrounding the Dangkor Municipal Solid Waste Landfill </h5>
                        <h6 class="card-subtitle2 mb-2 text-muted">SRENG Soknet & PROUM Sorya</h6>
                        <h6 class="card-subtitle2 mb-2 text-muted">pages: 97–124</h6>
                        <h6 class="card-subtitle2 mb-2 text-muted"><strong> Published online:</strong> 30 June 2019</h6>
                        <a href="/files/CJABJ-Issue-1-Number-1/CJABJ-Issue-1-Number-1_paper 5.pdf" class="card-link" download>PDF download</a>
                      </div>
                    
                </div>
            </div>
            <div class="accordion" id="accordionExample">
                
                <div class="card mt-3">
                    <div class="card-body">
                        <h6 class="card-subtitle text-muted">Article</h6>
                        <h5 class="card-title">An economic assessment of urban flooding in Cambodia: A case study of Phnom Penh </h5>
                        <h6 class="card-subtitle2 mb-2 text-muted">KHAN Lyna</h6>
                        <h6 class="card-subtitle2 mb-2 text-muted">pages: 125–149</h6>
                        <h6 class="card-subtitle2 mb-2 text-muted"><strong> Published online:</strong> 30 June 2019</h6>
                        <a href="/files/CJABJ-Issue-1-Number-1/CJABJ-Issue-1-Number-1_paper 6.pdf" class="card-link" download>PDF download</a>
                      </div>
                    
                </div>
            </div>
            <div class="accordion" id="accordionExample">
                
                <div class="card mt-3">
                    <div class="card-body">
                        <h6 class="card-subtitle text-muted">Article</h6>
                        <h5 class="card-title">Lessons learned from the implementation of University Research Grants (2017−2018) at the Royal University of Phnom Penh</h5>
                        <h6 class="card-subtitle2 mb-2 text-muted">CHET Chealy</h6>
                        <h6 class="card-subtitle2 mb-2 text-muted">pages: 150–165</h6>
                        <h6 class="card-subtitle2 mb-2 text-muted"><strong> Published online:</strong> 30 June 2019</h6>
                        <a href="/files/CJABJ-Issue-1-Number-1/CJABJ-Issue-1-Number-1_paper 7.pdf" class="card-link" download>PDF download</a>
                      </div>
                    
                </div>
            </div>

            <h5 class="issue-title mt-4">Book Review:</h5>
            <div class="accordion" id="accordionExample">
                
                <div class="card mt-3">
                    <div class="card-body">
                        <h5 class="card-title">Phil Benson, ‘Teaching and researching autonomy in language learning’. (Harlow, UK, Longman/Pearson Education, 2011, 283 pages, 123 USD (Hardcover) ISBN-13: 978-1408205013</h5>
                        <h6 class="card-subtitle2 mb-2 text-muted">KEUK Chan Narith* & LIM Chantha</h6>
                        <h6 class="card-subtitle2 mb-2 text-muted">pages: 166–174</h6>
                        <h6 class="card-subtitle2 mb-2 text-muted"><strong> Published online:</strong> 30 June 2019</h6>
                        <a href="/files/CJABJ-Issue-1-Number-1/CJABJ-Issue-1-Number-1_paper 8.pdf" class="card-link" download>PDF download</a>
                      </div>
                    
                </div>
            </div>
        </div>
    </div>

@endsection