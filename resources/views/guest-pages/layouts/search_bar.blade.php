
<div class="container pt-3 bg-light my-2 text-center border rounded">
        <form class="row align-items-center p-10" action="{{URL::to('/search')}}" method="get">
          <div class=" form-group col-md-3 ml-auto">
            <h4 class="text-info">Search the article</h4>
          </div>
          <div class="form-group col-md-3 ml-auto">
          <input type="text" class="form-control form-lg text-center" value="{{ request()->input('title') }}" name="title" placeholder="Title">
          </div>
          <div class=" form-group col-md-3 ml-auto">
            <input type="text" class="form-control form-lg text-center" value="{{ request()->input('author') }}" name="author" placeholder="Author">
          </div>
          <div class="form-group col-md-3 ml-auto">
            <button type="submit" class="form-control btn btn-info btn-block "> <i class="fa fa-search" aria-hidden="true"></i> Search</button>
          </div>
        </form>
</div>

