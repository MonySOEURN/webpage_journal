<footer class="" >
      <a href="#top" class="smoothscroll scroll-top">
        <span class="icon-keyboard_arrow_up"></span>
      </a>

      <div class="container">
            <div class="row">
                <div class="col-lg-5 ">
                    <h5 class="text-center text-white"><ins>Informations</ins></h5>
                    <hr>
                    <ul  >
                        <a class="text-white" href="http://www.rupp.edu.kh/"><li>Royal University of Phnom Penh</li></a>
                        <a class="text-white" href="#"><li>Policy on Research Development and Developm ent </li></a>
                        <a class="text-white" href="#"><li>SOP for Human Subjects and Research Protocol</li></a>
                        <a class="text-white" href="#"><li>SOP for RUPP Research Management </li></a>
                    </ul>
                    <hr>

                </div>
                <div class="col-lg-7 ">
                    <img class=""  src="{{asset('images/2.jpg')}}"  alt="Image" style="width:600px; height:200px">
                </div>
            </div>
            <div class="text-center mt-4 pb-4">
                <i class="text-white" >Copyright © Research Office of the Royal University of Phnom Penh </i>
            </div>
      </div>
</footer>
