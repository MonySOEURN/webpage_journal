
    <div class="container">
        <div class="row">
            <div class="col-lg-4 ">
                <div class="mt-2  mb-2 text-center" >
                    <img class="img-thumbnail border rounded text-center" src="{{asset('images/journal_cover.jpg')}}"  style="width:200px; height:300px" />
                </div>
            </div>

            <div class="col-lg-8 mt-2  text-center">
                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel" >
                    <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                    </ol>
                    <div class="carousel-inner ">
                    <div class="carousel-item active ">
                    <img class="img-slide img-thumbnail" src="{{asset('images/1.jpg')}}"  alt="Image" >
                    </div>
                    <div class="carousel-item">
                        <img class="img-slide img-thumbnail" src="{{asset('images/2.jpg')}}"  alt="Image">
                    </div>
                    <div class="carousel-item">
                        <img class=" img-slide img-thumbnail"  src="{{asset('images/3.jpg')}}"  alt="Image">
                    </div>

                    <div class="carousel-item">
                        <img class="img-slide img-thumbnail" src="{{asset('images/3.jpg')}}"  alt="Image">
                    </div>

                    <div class="carousel-item">
                        <img class="img-slide img-thumbnail" src="{{asset('images/4.jpg')}}"  alt="Image">
                    </div>

                    <div class="carousel-item">
                        <img class="img-slide img-thumbnail" src="{{asset('images/5.jpg')}}"  alt="Image">
                    </div>
                    </div>

                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
        </div>
    </div>

<style>
    img.img-slide
    {
       height:300px;
    }
</style>
