<div class="col-lg-4 pl-0">
  <div class="bg-light p-3 border rounded mb-4">
      <div class="col-12">
          <a  href = "/submit_journal"><button class="btn btn-block btn-white border-info btn-md text-info" >Submit to CJBAR</button></a>
      </div>
      <hr />
      <h3 class="text-info mt-3 h5 pl-3 mb-3">Journal menu</h3>
      <ul>
          <a class="text-info" href="/"><li>Home</li></a>
          <a class="text-dark" href="/aim_and_scope"><li>Aim and Scope</li></a>
          <a class="text-dark" href="/instruction"><li>Instruction for author</li></a>
          <a class="text-dark" href="/issue"><li>Special Issue</li></a>
          <a class="text-dark" href="/editorial_board_member"><li>Editorial Board Member</li></a>
          <a class="text-dark" href="/board"><li>Editorial Board</li></a>
          <a class="text-dark" href="/policy"><li>Editorial Policy</li></a>
      </ul>
  </div>
</div>
