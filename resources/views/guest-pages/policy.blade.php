
@extends('app')

@section('content')

  <div class="col-lg-8 bg-light">
      <div class="mb-5">
        <figure class="mb-5"> </figure>
        <h3 class="h5 d-flex align-items-center mb-4 text-info"><span class="icon-align-left mr-3"></span>Editorial Policy</h3>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Debitis illum fuga eveniet. Deleniti asperiores, commodi quae ipsum quas est itaque, ipsa, dolore beatae voluptates nemo blanditiis iste eius officia minus.</p>
        <p>Velit unde aliquam et voluptas reiciendis non sapiente labore, deleniti asperiores blanditiis nihil quia officiis dolor vero iste dolore vel molestiae saepe. Id nisi, consequuntur sunt impedit quidem, vitae mollitia!</p>
      </div>
      <div class="mb-5">
        <h3 class="h5 d-flex align-items-center mb-4 text-primary">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Debitis illum fuga eveniet</h3>

      </div>
      <div class="mb-5">
        <h3 class="h5 d-flex align-items-center mb-4 text-primary">Velit unde aliquam et voluptas reiciendis non sapiente labore, deleniti asperiores blanditiis nihi</h3>
      </div>
      <div class="mb-5">
        <h3 class="h5 d-flex align-items-center mb-4 text-primary">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Debitis illum fuga eveniet</h3>

      </div>
      <div class="mb-5">
        <h3 class="h5 d-flex align-items-center mb-4 text-primary">Velit unde aliquam et voluptas reiciendis non sapiente labore, deleniti asperiores blanditiis nihi</h3>
      </div>

  </div>

@endsection