
@extends('app')

@section('content')

  <div class="col-lg-8 bg-light">
      <div class="mb-5">
        <h3 class="h5 d-flex align-items-center mb-4 text-info"><span class="icon-align-left mr-3"></span>Editors</h3>
      </div>
      <div class="mb-5">
        <h3 class="h5 d-flex align-items-center mb-4" style="color: #0026F5;"><span class="icon-people mr-3"></span><b>Dr. Sok Serey</b>, The Hong Kong Baptist University, Hong Kong </h3>
        <ul class=" ">
          <li class="d-flex align-items-start mb-2"><span>Deputy Head of Research Office</span></li>
          <li class="d-flex align-items-start mb-2"><span>Royal University of Phnom Penh (RUPP)</span></li>
          <li class="d-flex align-items-start mb-2"><i>Editor-in-Chief</i></li>
        </ul>
      </div>
      <div class="mb-5">
        <h3 class="h5 d-flex align-items-center mb-4 " style="color: #0026F5;"><span class="icon-people mr-3"></span><b>Dr.Un Leng</b>, University of Amsterdam, The Netherlands </h3>
        <ul class=" ">
          <li class="d-flex align-items-start mb-2"><span>Dean of Faculty of Social Sciences and Humanities</span></li>
          <li class="d-flex align-items-start mb-2"><span>Royal University of Phnom Penh (RUPP)</span></li>
          <li class="d-flex align-items-start mb-2"><i>Section Editor-in-Chief</i></li>
          <li class="d-flex align-items-start mb-2"><i>Social Sciences and Humanities</i></li>
        </ul>
      </div>
      <div class="mb-5">
        <h3 class="h5 d-flex align-items-center mb-4 " style="color: #0026F5;"><span class="icon-people mr-3"></span><b>Dr. Chey Chan Oeurn</b>, Linköping University, Sweden </h3>
        <ul class=" ">
          <li class="d-flex align-items-start mb-2"><span>Vice-Dean of Faculty of Science</span></li>
          <li class="d-flex align-items-start mb-2"><span>Royal University of Phnom Penh (RUPP)</span></li>
          <li class="d-flex align-items-start mb-2"><i>Section Editor-in-Chief</i></li>
          <li class="d-flex align-items-start mb-2"><i>Science and Engineering </i></li>
        </ul>
      </div>

      <div class="mb-5">
        <h3 class="h5 d-flex align-items-center mb-4 " style="color: #0026F5;"><span class="icon-people mr-3"></span><b>Dr. Sok Soth</b>, Victoria University, Australia </h3>
        <ul class=" ">
          <li class="d-flex align-items-start mb-2"><span>Dean of Faculty of Education </span></li>
          <li class="d-flex align-items-start mb-2"><span>Royal University of Phnom Penh (RUPP)</span></li>
          <li class="d-flex align-items-start mb-2"><i>Section Editor-in-Chief</i></li>
          <li class="d-flex align-items-start mb-2"><i>Education, linguistic and languages </i></li>
        </ul>
      </div>

      <div class="mb-5">
        <h3 class="h5 d-flex align-items-center mb-4 " style="color: #0026F5;"><span class="icon-people mr-3"></span><b>Dr. Pheakdey Nguonphan</b>, University of Heidelberg, Germany</h3>
        <ul class=" ">
          <li class="d-flex align-items-start mb-2"><span>Dean of Faculty of Engineering </span></li>
          <li class="d-flex align-items-start mb-2"><span>Royal University of Phnom Penh (RUPP)</span></li>
          <li class="d-flex align-items-start mb-2"><i>Section Editor-in-Chief</i></li>
          <li class="d-flex align-items-start mb-2"><i>Architecture and Engineering </i></li>
        </ul>
      </div>

      <div class="mb-5">
        <h3 class="h5 d-flex align-items-center mb-4 " style="color: #0026F5;"><span class="icon-people mr-3"></span><b>Dr. Lay Chanthy</b>, Asian Institute of Technology, Thailand </h3>
        <ul class=" ">
          <li class="d-flex align-items-start mb-2"><span>Deputy Head of Research Office</span></li>
          <li class="d-flex align-items-start mb-2"><span>Royal University of Phnom Penh (RUPP)</span></li>
          <li class="d-flex align-items-start mb-2"><i>Section Editor-in-Chief</i></li>
          <li class="d-flex align-items-start mb-2"><i>Environment and Climate Change </i></li>
        </ul>
      </div>

      <div class="mb-5">
        <h3 class="h5 d-flex align-items-center mb-4 t" style="color: #0026F5;"><span class="icon-people mr-3"></span><b>Dr. Chhinh Nyda</b>, Flinders University, Australia</h3>
        <ul class=" ">
          <li class="d-flex align-items-start mb-2"><span>Lecturer</span></li>
          <li class="d-flex align-items-start mb-2"><span>Faculty of Development Studies </span></li>
          <li class="d-flex align-items-start mb-2"><span>Royal University of Phnom Penh (RUPP)</span></li>
          <li class="d-flex align-items-start mb-2"><i>Section Editor-in-Chief</i></li>
          <li class="d-flex align-items-start mb-2"><i>Physical Geography, GIS and Remote Sensing </i></li>
        </ul>
      </div>

      <div class="mb-5">
        <h3 class="h5 d-flex align-items-center mb-4 " style="color: #0026F5;"><span class="icon-people mr-3"></span><b>Dr. Sou Veasna</b>, National Cheng Kung University, Taiwan </h3>
        <ul class=" ">
          <li class="d-flex align-items-start mb-2"><span>Researcher of Research Office</span></li>
          <li class="d-flex align-items-start mb-2"><span>Royal University of Phnom Penh (RUPP)</span></li>
          <li class="d-flex align-items-start mb-2"><i>Section Editor-in-Chief</i></li>
          <li class="d-flex align-items-start mb-2"><p>International Business and Management </p></li>
        </ul>
      </div>

  </div>

@endsection
