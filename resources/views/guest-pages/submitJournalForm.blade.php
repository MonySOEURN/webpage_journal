<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Journal</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="keywords" content="" />
        <meta name="author" content="Free-Template.co" />
        <link rel="stylesheet" href="css/custom-bs.css" />
        <link rel="stylesheet" href="css/jquery.fancybox.min.css" />
        <link rel="stylesheet" href="css/bootstrap-select.min.css" />
        <link rel="stylesheet" href="fonts/icomoon/style.css" />
        <link rel="stylesheet" href="fonts/line-icons/style.css" />
        <link rel="stylesheet" href="css/owl.carousel.min.css" />
        <link rel="stylesheet" href="css/animate.min.css" />
        <!-- MAIN CSS -->
        <link rel="stylesheet" href="css/style.css" />

        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
        <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    </head>
    <body id="top">
        <div class="site-wrap">
          <div style="background: url('') no-repeat fixed 100%; -webkit-background-size: cover;">
            <div class="site-mobile-menu site-navbar-target">
                <div class="site-mobile-menu-header">
                    <div class="site-mobile-menu-close mt-3">
                        <span class="icon-close2 js-menu-toggle"></span>
                    </div>
                </div>
                <div class="site-mobile-menu-body"></div>
            </div>
            <!-- .site-mobile-menu -->
            <div class="container col-sm-8" id="container">
                <form class="container  bg-light" action="" method="">
                    <div>
                        <h2 class="text-info text-center pt-4"><b>Submit the journal to CJBAR</b></h2>
                    </div>
                    <hr>
                    <div class="form-group">
                        <h3 class="text-dark" ><b>Stage 1: Type and Title</b></h3>
                    </div>
                    <div class="form-group">
                        <h5 class="text-dark" ><b>Type of Manuscript </b></h5>
                    </div>
                    <div class="form-group">
                        <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="original_paper">
                        <label class="form-check-label" for="gridCheck">
                            Original Paper
                        </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="policy_paper">
                        <label class="form-check-label" for="gridCheck">
                            Policy Paper
                        </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="form-check">
                        <input class="form-check-input" type="checkbox" name="book_review">
                        <label class="form-check-label" for="gridCheck">
                            Book Review
                        </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <i class="text-danger">(Note the submission is only allowed one type)</i>
                    </div>

                    <div class="form-group">
                        <h5 class="text-dark" ><b>Title of Manuscript </b></h5>
                    </div>
                    <div class="form-group">
                        <textarea class="form-control" id="exampleFormControlTextarea1" rows="2" name="type_menuscript"></textarea>
                    </div>
                    <hr>

                    <div class="form-group">
                        <h3 class="text-dark" ><b>Stage 2: Author Details </b></h3>
                    </div>

                    <div class="container" >
                        <div>
                            <button type="button" class="btn btn-info mb-2" id="addForm">+ Add more author forms</button>
                        </div>
                        <div class="card mb-3" id="inputNewForm" >
                            <div class="card-body ">
                                <div class="form-group row">
                                    <label for="inputPassword" class="col-sm-3 col-form-label">Surname: </label>
                                    <div class="col-sm-9 ">
                                        <input type="text" class="form-control " id="inputPassword" placeholder="Enter the surname" name="surname[]">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputPassword" class="col-sm-3 col-form-label">First name: </label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control " id="inputPassword" placeholder="Enter the firstname" name="firstname[]">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <span class="text-dark" ><b>Prefix:</b></span>
                                    <i class="text-danger"> (Please give option as show such as Dr. Miss Mr.)</i>
                                </div>
                                <div class="form-group row">
                                    <label for="inputPassword" class="col-sm-3 col-form-label">Prefix: </label>
                                    <div class="col-sm-7">
                                        <select class="form-control " name="prefix[]">
                                            <option>Choose prefix...</option>
                                            <option value="Dr.">Dr.</option>
                                            <option value="Miss">Miss</option>
                                            <option value="Mr.">Mr.</option>
                                            <option value="Mrs">Mrs</option>
                                            <option value="Ms">Ms</option>
                                            <option value="professor">Professor</option>
                                            <option value="Mx">Mx</option>
                                            <option value="Ind">Ind</option>
                                        </select>
                                    </div>
                                    <i class="text-danger">Require*</i>
                                </div>
                                <div class="form-group row">
                                    <label for="inputPassword" class="col-sm-3 col-form-label">First name: </label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control " id="inputPassword" placeholder="Enter the firstname(Given name)" name="fname[]">
                                    </div>
                                    <i class="text-danger">Require*</i>
                                </div>
                                <div class="form-group row">
                                    <label for="inputPassword" class="col-sm-3 col-form-label">Middle name: </label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control " id="inputPassword" placeholder="Enter the middle name" name ="mname[]">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputPassword" class="col-sm-3 col-form-label">Last name: </label>
                                    <div class="col-sm-7">
                                        <input type="text" class="form-control " id="inputPassword" placeholder="Enter the last name(Family name)" name="lname[]" >
                                    </div>
                                    <i class="text-danger">Require*</i>
                                </div>
                                <div class="form-group row">
                                    <label for="inputPassword" class="col-sm-3 col-form-label">Degree: </label>
                                    <div class="col-sm-7">
                                        <select class="form-control " name="degree">
                                            <option>Choose degree...</option>
                                            <option value="bachelor">Bachelor Degree</option>
                                            <option value="master">Master Degree</option>
                                            <option value="phd">PhD Degree</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputPassword" class="col-sm-4 col-form-label">Primary Email: </label>
                                    <div class="col-sm-5">
                                        <input type="text" class="form-control " id="inputPassword" placeholder="Enter the primary email" name="pemail">
                                    </div>
                                    <i class="text-danger">Require*</i>
                                </div>
                                <div class="form-group row">
                                    <label for="inputPassword" class="col-sm-4 col-form-label"> Confirm primary email: </label>
                                    <div class="col-sm-5">
                                        <input type="text" class="form-control " id="inputPassword" placeholder="Confirm the primary email" name="confirm_pemail">
                                    </div>
                                    <i class="text-danger">Require*</i>
                                </div>
                                <div class="form-group row">
                                    <label for="inputPassword" class="col-sm-4 col-form-label"> Primary Cc Email : </label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control " id="inputPassword" placeholder="Enter the primary Cc email" name="pc_email">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputPassword" class="col-sm-4 col-form-label"> Secondary Email : </label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control " id="inputPassword" placeholder="Enter the secondary email" name="semail">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputPassword" class="col-sm-4 col-form-label"> Confirm secondary email: </label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control " id="inputPassword" placeholder="Confirm the secondary email" name="confirm_semail">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputPassword" class="col-sm-4 col-form-label"> Secondary Cc Email : </label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control " id="inputPassword" placeholder="Enter the secondary Cc email" name="sc_email">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputPassword" class="col-sm-4 col-form-label"> Affiliation: </label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control " id="inputPassword" placeholder="Enter the affiliation:" name="affiliation">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="inputPassword" class="col-sm-4 col-form-label"> Address of Affiliation: </label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control " id="inputPassword" placeholder="Enter the address of affiliation:" add_affiliation>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="newForm"></div>
                    </div>

                    <div class="form-group">
                        <h3 class="text-dark" ><b>Stage 3: Manuscript Details  </b></h3>
                    </div>
                    <div class="form-group row">
                        <label for="inputPassword" class="col-sm-4 col-form-label">Number of Figures:</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="inputPassword" placeholder="Enter the number of Figures:">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="inputPassword" class="col-sm-4 col-form-label">Number of tables:</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="inputPassword" placeholder="Enter the number of tables:">
                        </div>
                    </div>
                    <div class="form-group">
                        <p class="text-dark" >Has this manuscript been submitted previously to this journal?<span class="text-danger"><sup>*</sup></span></p>
                    </div>
                    <div class="form-group">
                        <div class="form-check">
                        <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="option1" checked>
                        <label class="form-check-label" for="gridCheck">
                            Yes
                        </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="form-check">
                        <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios2" value="option2">
                        <label class="form-check-label" for="gridCheck">
                            No
                        </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <p class="text-dark" >If yes, what is the manuscript ID of the previous submission?</p>
                    </div>
                    <div class="form-group">
                        <input class="form-control" id="exampleFormControlTextarea1" >
                    </div>
                    <div class="form-group">
                        <p class="text-dark" >Confirm the following:</p>
                    </div>
                    <div class="form-group">
                        <div class="form-check">
                        <input class="form-check-input" type="checkbox" id="gridCheck">
                        <label class="form-check-label text-justify" for="gridCheck">
                            Confirm that the manuscript has been submitted solely to this journal and is not published, in press, or submitted elsewhere.
                        </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="form-check">
                        <input class="form-check-input" type="checkbox" id="gridCheck">
                        <label class="form-check-label text-justify" for="gridCheck">
                            Confirm that all the research meets the ethical guidelines, including adherence to the legal requirements of the study country.
                        </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="form-check">
                        <input class="form-check-input" type="checkbox" id="gridCheck">
                        <label class="form-check-label text-justify" for="gridCheck">
                            Confirm that you have prepared (a) a complete text and (b) a separate title page, containing all authors, institutional affiliations, full addresses and other contact informational, as well as any acknowledgements, to allow blinded review.
                        </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <p class="text-dark" >Do you have any conflict of interest?</p>
                    </div>
                    <div class="form-group">
                        <div class="form-check">
                        <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="option1" checked>
                        <label class="form-check-label" for="gridCheck">
                            Yes
                        </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="form-check">
                        <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios2" value="option2">
                        <label class="form-check-label" for="gridCheck">
                            No
                        </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <p class="text-dark" >If yes, please state:</p>
                    </div>
                    <div class="form-group">
                        <textarea class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                    </div>
                    <div class="form-group row">
                        <label for="inputPassword" class="col-sm-3 col-form-label">Choose journal file: </label>
                        <div class="col-sm-9">
                            <input type="file" class="form-control text-center" id="inputPassword" placeholder="Enter the middle name">
                        </div>
                    </div>

                    <button type="submit" class="btn btn-info mb-5">Submit</button>
                </form>
            </div>
        </div>
        <!-- SCRIPTS -->
        <script src="js/jquery.min.js"></script>
        <script src="js/bootstrap.bundle.min.js"></script>
        <script src="js/isotope.pkgd.min.js"></script>
        <script src="js/stickyfill.min.js"></script>
        <script src="js/jquery.fancybox.min.js"></script>
        <script src="js/jquery.easing.1.3.js"></script>

        <script src="js/jquery.waypoints.min.js"></script>
        <script src="js/jquery.animateNumber.min.js"></script>
        <script src="js/owl.carousel.min.js"></script>

        <script src="js/bootstrap-select.min.js"></script>

        <script src="js/custom.js"></script>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>


        <script type="text/javascript">
            $('#addForm').click(function(){
                var html = '';
                html+=
                        ' <div class="card mb-3" id="inputNewForm" >\
                            <div class="card-header"> \
                                <button type="button" class="btn btn-danger float-right " id="removeForm">X</button> \
                            </div> \
                            <div class="card-body ">\
                                <div class="form-group row"> \
                                    <label for="inputPassword" class="col-sm-3 col-form-label">Surname: </label> \
                                    <div class="col-sm-9 "> \
                                        <input type="text" class="form-control " id="inputPassword" placeholder="Enter the surname" name="surname"> \
                                    </div>\
                                </div> \
                                <div class="form-group row"> \
                                    <label for="inputPassword" class="col-sm-3 col-form-label">First name: </label> \
                                    <div class="col-sm-9"> \
                                        <input type="text" class="form-control " id="inputPassword" placeholder="Enter the firstname" name="firstname"> \
                                    </div> \
                                </div> \
                                <div class="form-group"> \
                                    <span class="text-dark" ><b>Prefix:</b></span> \
                                    <i class="text-danger"> (Please give option as show such as Dr. Miss Mr.)</i> \
                                </div> \
                                <div class="form-group row"> \
                                    <label for="inputPassword" class="col-sm-3 col-form-label">Prefix: </label> \
                                    <div class="col-sm-7"> \
                                        <select class="form-control " name="prefix"> \
                                            <option>Choose prefix...</option> \
                                            <option value="Dr.">Dr.</option> \
                                            <option value="Miss">Miss</option> \
                                            <option value="Mr.">Mr.</option> \
                                            <option value="Mrs">Mrs</option> \
                                            <option value="Ms">Ms</option> \
                                            <option value="professor">Professor</option> \
                                            <option value="Mx">Mx</option> \
                                            <option value="Ind">Ind</option> \
                                        </select> \
                                    </div> \
                                    <i class="text-danger">Require*</i> \
                                </div> \
                                <div class="form-group row"> \
                                    <label for="inputPassword" class="col-sm-3 col-form-label">First name: </label> \
                                    <div class="col-sm-7"> \
                                        <input type="text" class="form-control " id="inputPassword" placeholder="Enter the firstname(Given name)"> \
                                    </div> \
                                    <i class="text-danger">Require*</i> \
                                </div> \
                                <div class="form-group row"> \
                                    <label for="inputPassword" class="col-sm-3 col-form-label">Middle name: </label> \
                                    <div class="col-sm-7"> \
                                        <input type="text" class="form-control " id="inputPassword" placeholder="Enter the middle name"> \
                                    </div> \
                                </div> \
                                <div class="form-group row"> \
                                    <label for="inputPassword" class="col-sm-3 col-form-label">Last name: </label> \
                                    <div class="col-sm-7"> \
                                        <input type="text" class="form-control " id="inputPassword" placeholder="Enter the last name(Family name)"> \
                                    </div> \
                                    <i class="text-danger">Require*</i> \
                                </div> \
                                <div class="form-group row"> \
                                    <label for="inputPassword" class="col-sm-3 col-form-label">Degree: </label> \
                                    <div class="col-sm-7"> \
                                        <select class="form-control "> \
                                            <option>Choose degree...</option> \
                                            <option>Bachelor Degree</option> \
                                            <option>Master Degree</option> \
                                            <option>PhD Degree</option> \
                                        </select> \
                                    </div> \
                                </div> \
                                <div class="form-group row"> \
                                    <label for="inputPassword" class="col-sm-4 col-form-label">Primary Email: </label> \
                                    <div class="col-sm-5"> \
                                        <input type="text" class="form-control " id="inputPassword" placeholder="Enter the primary email"> \
                                    </div> \
                                    <i class="text-danger">Require*</i> \
                                </div> \
                                <div class="form-group row"> \
                                    <label for="inputPassword" class="col-sm-4 col-form-label"> Confirm primary email: </label> \
                                    <div class="col-sm-5"> \
                                        <input type="text" class="form-control" id="inputPassword" placeholder="Confirm the primary email"> \
                                    </div> \
                                    <i class="text-danger">Require*</i> \
                                </div> \
                                <div class="form-group row"> \
                                    <label for="inputPassword" class="col-sm-4 col-form-label"> Primary Cc Email : </label> \
                                    <div class="col-sm-6"> \
                                        <input type="text" class="form-control " id="inputPassword" placeholder="Enter the primary Cc email"> \
                                    </div> \
                                </div> \
                                <div class="form-group row"> \
                                    <label for="inputPassword" class="col-sm-4 col-form-label"> Secondary Email : </label> \
                                    <div class="col-sm-6"> \
                                        <input type="text" class="form-control " id="inputPassword" placeholder="Enter the secondary email"> \
                                    </div> \
                                </div> \
                                <div class="form-group row"> \
                                    <label for="inputPassword" class="col-sm-4 col-form-label"> Confirm secondary email: </label> \
                                    <div class="col-sm-6"> \
                                        <input type="text" class="form-control " id="inputPassword" placeholder="Confirm the secondary email"> \
                                    </div> \
                                </div> \
                                <div class="form-group row"> \
                                    <label for="inputPassword" class="col-sm-4 col-form-label"> Secondary Cc Email : </label> \
                                    <div class="col-sm-6"> \
                                        <input type="text" class="form-control " id="inputPassword" placeholder="Enter the secondary Cc email"> \
                                    </div> \
                                </div> \
                                <div class="form-group row"> \
                                    <label for="inputPassword" class="col-sm-4 col-form-label"> Affiliation: </label> \
                                    <div class="col-sm-6"> \
                                        <input type="text" class="form-control " id="inputPassword" placeholder="Enter the affiliation:"> \
                                    </div> \
                                </div> \
                                <div class="form-group row"> \
                                    <label for="inputPassword" class="col-sm-4 col-form-label"> Address of Affiliation: </label> \
                                    <div class="col-sm-6"> \
                                        <input type="text" class="form-control " id="inputPassword" placeholder="Enter the address of affiliation:"> \
                                    </div> \
                                </div> \
                            </div>\
                        </div>'
                        ;

            $('#newForm').append(html);

            });
            $(document).on('click','#removeForm',function(){
                $(this).closest('#inputNewForm').remove();

            });

        </script>



    </body>
</html>

</script>


