@extends('app')

@section('content')

  <div class="col-lg-8 bg-light">
      <div class="mb-5">
        <figure class="mb-5"> </figure>
        <h3 class="h5 d-flex align-items-center mb-4" style="color: #0026F5;"><span class="icon-align-left mr-3"  style="color: #0026F5;"></span>About the CJBAR</h3>
        <p class="text-justify">
            Insight: The Cambodia Journal of Basic and Applied Research (CJBAR) has been approved by the Ministry of Information (MoI) upon the Royal University of Phnom Penh or RUPP’s request, No. 770/2018 RUPP, dated on 16 August 2018.
            The preparation and printing of the Journal is generously supported by the Research Office of the RUPP, who also provided technical support in hosting, editing, and publishing the CJBAR. The RUPP is the oldest and the largest public university in Cambodia.
            It has contributed significantly to development of human resources for many sectors, especially to the training of teachers in high schools and other public servants.
            With respect to Cambodia’s integration into the ASEAN Economic Community in 2015, the role of RUPP in furthering the scope of these achievements will need to be improved and strengthened accordingly.
            A situation analysis has shown that RUPP faces with considerable challenges in the further development of capacity in Cambodia, requiring comprehensive reform in terms of leadership and management, administration, and financing of the university.
            Our goal is to position the right people in the right place, improving teaching and learning methods, enhancing institutional capacity for research and development, and developing infrastructure and campus services.
              We believe this is essential for transforming RUPP into the flagship public university in Cambodia. Currently, 6% of our full-time staff hold doctoral degrees, 64% have master’s degrees and 93% of our administration and finance staff are qualified to at least a bachelor degree level.
              We have almost 20,000 students and have more students receiving support from scholarships and fee waivers than any other large publicly funded university in Cambodia.
        </p>
      </div>
      <div class="mb-5">
        <h3 class="h5 d-flex align-items-center mb-4" style="color: #0026F5;"><span class="icon-rocket mr-3" style="color: #0026F5;"></span>Aims and Scope</h3>
        <p class="text-justify">
            Insight: Cambodia Journal of Basic and Applied Research (CJBAR) is an academic, policy and practice-oriented journal, which covers all aspects of science and engineering, the social sciences and humanities, education, development studies, and languages.
            Academic or applied research manuscripts from within Cambodia; or from outside Cambodia but contributing to the social, economic, or environmental development of Cambodia, ASEAN, or the Greater Mekong Subregion may be submitted to the journal.
            The journal welcomes manuscripts from any discipline, where theories, concepts, innovations, new technologies, or best practices are introduced. However, the journal reserves the right to prioritize research focused on topics aligned with the courses offered at RUPP.

        </p>
        <p class="text-justify">
            This is a bilingual Journal where an author many decide to publish his/her manuscript either in English or Khmer. However, all manuscripts published in the CJBAR must have abstracts in both English and Khmer.
              The Journal publishes two issues per annum:
        </p>
        <ul>
          <li><span>Issue 1: January-June</span></li>
          <li><span>Issue 2: July- December</span></li>
        </ul>
      </div>
      <div class="mb-5">
        <h3 class="h5 d-flex align-items-center mb-4" style="color: #0026F5;"><span class="icon-turned_in mr-3" style="color: #0026F5;"></span>Peer Review Integrity</h3>
        <p class="text-justify">
            All manuscripts considered for publication will undergo a process of double-blind peer review by independent expert referees. An internal evaluation will occur before the peer review process. The editor will inform the author whether the manuscript has been accepted or rejected within one month of the completion of the peer-review process.
            In total, the internal evaluation and external peer-review process may take up to four months, whereby the result may be:
        </p>
        <ul class="text-justify">
          <li><span><strong> Accepted without changes.</strong> If a manuscript is evaluated as <span style="font-style: italic">“Accepted without change”</span>, it is published in its original form. </span></li>
          <li><span><strong> Accepted with minor changes.</strong> If a manuscript is evaluated as <span style="font-style: italic">“Accepted with minor changes”</span>, the editor will accept the submission on revision of some minor aspects of the paper.</span></li>
          <li><span><strong> Accepted with major changes.</strong> If a manuscript is evaluated as <span style="font-style: italic">“Accepted with major changes”</span>, it will require an additional external evaluation by the peer review team.</span></li>
          <li><span><strong> Rejected.</strong> If a manuscript is evaluated as <span style="font-style: italic">“Rejected”</span>, it will not be accepted for re-submission, regardless of how it is later revised. </span></li>
        </ul>
        <p class="text-justify">
            When the author is notified of acceptance to the journal, the editor will forward comments from the two peer-reviewers for consideration. Where a manuscript requires changes, the author will be given 30 days to review the manuscript.
              The author may be offered one or more rounds of review to change the manuscript meet the minimum standards required for publication in the journal.
        </p>
      </div>

      <div class="mb-5">
        <h3 class="h5 d-flex align-items-center mb-4" style="color: #0026F5;"><span class="icon-turned_in mr-3" style="color: #0026F5;"></span>Author Instruction </h3>
        <h6 style="color: #0026F5;"><strong> Manuscripts Accepted</strong></h6>
        <h6 style="color: #0026F5;"><strong> Natural Science, Science, and Engineering</strong></h6>
        <ul>
          <li><span>Original/research paper (4,000–6,000 words, including references)</span></li>
          <li><span>Short communication/policy paper (3,000–4,000 words, including references)</span></li>
          <li><span>News (<1000 words)</span></li>
        </ul>
        <h6><strong>Humanities, Social Science, Development Studies, Education, and Languages</strong></h6>
        <ul>
          <li><span>Original/research paper (6,000–10,000 words, including references)</span></li>
          <li><span>Short communication/policy paper (3,000–4,000 words, including references)</span></li>
          <li><span>News (<1000 words)</span></li>
        </ul>
        <h6 style="color: #0026F5;"><strong> Paper Structure</strong></h6>
        <p class="text-justify">
            <strong> An original research paper</strong> may vary in structure based on specific requirements of the topic, or the disciplinary background of the author(s), in terms of experiments, surveys, case studies, and so on. However, authors will need to collect and analyze new data and conduct an original study.
            The paper should be based on analysis and interpretation of this data. The suggested structure for a paper in this category is as follows:
        </p>
        <ul>
          <li>Introduction,</li>
          <li>Conceptual framework/literature review,</li>
          <li>Methodology,</li>
          <li>Discussion,</li>
          <li>Conclusion,</li>
          <li>References, and </li>
          <li>Supplementary Data (if required).</li>
        </ul>

        <p class="text-justify">
            <strong>A book review </strong>is an analysis of a book, including its contents, style, and merit.
              A book review may be written as an opinion piece, summary, or scholarly review. The suggested structure for a book review is as follows:
        </p>
        <ul>
          <li>Introduction,</li>
          <li>Body (a review of the contents of the book),</li>
          <li>Analysis and evaluation,</li>
          <li>Conclusion, and</li>
          <li>References.</li>
        </ul>

        <p class="text-justify">
            <strong>A short policy paper </strong>is a communication piece focusing on a specific policy issue, which provides clear recommendations for policymakers. It is generally a preliminary study, which is a precursor to an original research paper.
              A manuscript for a short policy paper should use  the following structure:
        </p>
        <ul>
          <li>Abstract,</li>
          <li>Introduction,</li>
          <li>Research methodology,</li>
          <li>Results, </li>
          <li>Discussion,</li>
          <li>References, and</li>
          <li>Supplementary Data (if required).</li>
        </ul>

        <p class="text-justify">
            <strong>A news item </strong>is a brief description of a research project and an outline of some preliminary results.
              This provides an opportunity for authors to disseminate important results quickly. A news item should adhere to the following structure:
        </p>
        <ul>
          <li>Introduction,</li>
          <li>Aims and objectives,</li>
          <li>Research methodology,</li>
          <li>Preliminary results (if relevant),</li>
          <li>Discussion, and </li>
          <li>References.</li>
        </ul>
      </div>
      <h6 style="color: #0026F5;"><strong>Journal Structure </strong></h6>
      <p class="text-justify">
          The Journal is publishing 1 volume including 2 issues per year. One issue includes: 1 editorial, 5 original/research papers, 1 policy paper and book review.
          The outline of each issue is structured as follows:
      </p>
      <ul>
        <li>Editorial, </li>
        <li>Original/research Paper 1,</li>
        <li>Original/research Paper 2,</li>
        <li>Original/research Paper 3,</li>
        <li>Original/research Paper 4,</li>
        <li>Original/research Paper 5,</li>
        <li>Policy Paper, and</li>
        <li>Book Review. </li>
      </ul>

      <h6 style="color: #0026F5;"><strong>Papers from Field of Study</strong></h6>
      <h6><strong>Science and Engineering</strong></h6>
      <ul>
        <li>Biology</li>
        <li>Chemistry</li>
        <li>Computer science</li>
        <li>Environmental science</li>
        <li>Climate change </li>
        <li>Biodiversity conservation</li>
        <li>Mathematics</li>
        <li>Physics</li>
        <li>IT Engineering</li>
        <li>Information technology engineering</li>
        <li>Telecommunication and electronic engineering</li>
        <li>Bioengineering</li>
      </ul>

      <h6><strong>Social Science and Humanities </strong></h6>
      <ul>
        <li>Geography</li>
        <li>History</li>
        <li>Khmer literature</li>
        <li>Media and communication</li>
        <li>Philosophy</li>
        <li>Psychology</li>
        <li>Sociology</li>
        <li>Social work</li>
        <li>Tourism and resource management </li>
        <li>IClinical psychology</li>
      </ul>

      <h6><strong>Development Studies </strong></h6>
      <ul>
        <li>Community development </li>
        <li>Economic development</li>
        <li>Natural resource management and development </li>
      </ul>

      <h6><strong>Education and Languages </strong></h6>
      <ul>
        <li>Educational studies</li>
        <li>Higher education development and management</li>
        <li>Lifelong learning</li>
        <li>Educational research and training</li>
        <li>International studies </li>
        <li>Linguistics</li>
        <li>TESOL</li>
        <li>Translation science</li>
        <li>Chinese </li>
        <li>Thai</li>
        <li>English </li>
        <li>French </li>
        <li>Japanese </li>
        <li>Korean</li>
      </ul>

      <div class="mb-5">
        <h3 class="h5 d-flex align-items-center mb-4" style="color: #0026F5;"><span class="icon-turned_in mr-3" style="color: #0026F5;"></span>Publication Charges</h3>
        <p class="text-justify">
            There are no submission fees, publication fees or page charges for this journal. Submitted papers should be well formatted and use good English or Khmer.
            The Research Office of the RUPP covers the cost of peer-reviewed process, English editing, and printing the Journal.
            The author is responsible for translating the abstract from English into Khmer if she or he is not Khmer native.
        </p>
      </div>

      <div class="mb-5">
        <h3 class="h5 d-flex align-items-center mb-4" style="color: #0026F5;"><span class="icon-turned_in mr-3" style="color: #0026F5;"></span>Referencing Style</h3>
        <p class="text-justify">
            Insight: Cambodia Journal of Basic and Applied Research (CJBAR) has adopted the 7th Education of American Psychological Association (APA) style as referencing style.
            The example below is derived from <a href="https://middlebury.libguides.com/citation/apa7"> https://middlebury.libguides.com/citation/apa7</a>.
        </p>
      </div>

      <div class="mb-5">
        <p class="text-light" style="background-color: #19395C">Print Book Examples</p>
        <strong>Author, A. A. (Year of publication). Title of work. Publisher.</strong>
        <p class="text-justify">
            Calfee, R. C., & Valencia, R. R. (1991). APA guide to preparing manuscripts for journal publication. American Psychological Association.
        </p>
      </div>

      <div class="mb-5">
        <p class="text-light" style="background-color: #19395C">E-Book Example</p>
        <strong>Author, A. A. (Year of publication). Title of work. [Ebook]. Publisher. Nondatabase URL</strong>
        <p class="text-justify">
            De Huff, E. W. (n.d.). Taytay’s tales: Traditional Pueblo Indian tales. [Ebook]. <a href=" http://digital.library.upenn.edu/women/dehuff/taytay/taytay.html"> http://digital.library.upenn.edu/women/dehuff/taytay/taytay.html</a>
        </p>
      </div>

      <div class="mb-5">
        <p class="text-light" style="background-color: #19395C">Print Journal Example</p>
        <strong>Author, A. A., Author, B. B., & Author, C. C. (Year). Title of article. Title of Periodical, volume number(issue number), page range.</strong>
        <p class="text-justify">
          Harlow, H. F. (1983). Fundamentals for preparing psychology journal articles. Journal of Comparative and Physiological Psychology, 55(2), 893-896.
        </p>
      </div>

      <div class="mb-5">
        <p class="text-light" style="background-color: #19395C">Online Journal Example</p>
        <strong>Author, A. A., & Author, B. B. (Date of publication). Title of article. Title of Journal, volume number(issue number), page range.</strong><a href="https://doi.org/">https://doi.org/</a>
        <p class="text-justify">
            Brownlie, D. (2007). Toward effective poster presentations: An annotated bibliography. European Journal of Marketing, 41(3), 1245-1283. <a href="https://doi.org/10.1108/03090560710821161"> https://doi.org/10.1108/03090560710821161</a>
        </p>
      </div>

      <div class="mb-5">
        <p class="text-light text-justify" style="background-color: #19395C">Print Magazine Example</p>
        <strong>Cite like a print journal article, but give the year and the month for monthly magazines. Add the day for weekly magazines.</strong>
        <p>
            Henry, W. A., III. (1990, April 9). Making the grade in today's schools. Time, 135, 28-31.
        </p>
      </div>

      <div class="mb-5">
        <p class="text-light" style="background-color: #19395C">Online Magazine Example</p>
        <strong>Cite like a print magazine article, except: a) no page numbers, and b) add a DOI (preferred) or full URL.</strong>
        <p class="text-justify">
            Auerback, M. (2019, January 27). In antitrust, size isn’t everything. Salon. <a href="https://www.salon.com/2019/01/27/in-antitrust-size-isnt-everything_partner/"> https://www.salon.com/2019/01/27/in-antitrust-size-isnt-everything_partner/</a>
          </p>
      </div>

      <div class="mb-5">
        <p class="text-light" style="background-color: #19395C">Online Newspaper Example</p>
        <strong>Cite like an online magazine article except that in most cases, you'll include a URL because no DOI will be available.
            <br> Author, A. A. (Year, Month Day). Title of article. Title of Newspaper. URL</strong>
        <p>
            Parker-Pope, T. (2008, May 6). Psychiatry handbook linked to drug industry. The New York Times. <a href="https://well.blogs.nytimes.com/2008/05/06/psychiatry-handbook-linked-to-drug-industry/"> https://well.blogs.nytimes.com/2008/05/06/psychiatry-handbook-linked-to-drug-industry/</a>
        </p>
      </div>

      <div class="mb-5">
        <p class="text-light" style="background-color: #19395C">Website Example</p>
        <strong>Author, A. A., & Author, B. B. (Date of publication). Title of page. Title of Website. URL</strong>
        <p class="text-justify">
            Martin Lillie, C. M. (2016, December 29). <i> Be kind to yourself: How self-compassion can improve your resiliency</i>. Mayo Clinic. <a href="https://www.mayoclinic.org/healthy-lifestyle/adult-health/in-depth/self-compassion-can-improve-your-resiliency/art-20267193"> https://www.mayoclinic.org/healthy-lifestyle/adult-health/in-depth/self-compassion-can-improve-your-resiliency/art-20267193</a>
            Cleveland Clinic. (2019, July 16). <i>Stress: 10 ways to ease stress.</i> <a href="https://my.clevelandclinic.org/health/articles/8133-stress-10-ways-to-ease-stress">https://my.clevelandclinic.org/health/articles/8133-stress-10-ways-to-ease-stress</a>
        </p>
      </div>

  </div>

@endsection
