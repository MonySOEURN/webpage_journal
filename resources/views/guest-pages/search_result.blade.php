
@extends('app')

@section('content')

    <div class="col-lg-8 bg-light">
        <div class="mb-2 mt-2">
            <p><strong>Search Result</strong>: found {{$total}}</p>
        </div>
        @foreach ($results as $result)
            <div class="mb-3">
                <div class="card w-100">
                    <div class="card-body">
                        <h5 class="card-title text-black">{{$result->title}}</h5>
                        <p>{{$result->name}}</p>
                        <p><strong> Published online: </strong>{{$result->date}}</p>
                        <a href="/images/myw3schoolsimage.jpg" download>Download Article as PDF</a>
                    </div>
                </div>
            </div>
        @endforeach
        <div class="mb-4">
            {{$results->links("pagination::bootstrap-4")}}
        </div>
        <br>
    </div>

@endsection
