
@extends('app')

@section('content')

<style>
nav > .nav.nav-tabs{

border: none;
  color:#fff;
  background:#272e38;
  border-radius:0;

}
nav > div a.nav-item.nav-link,
nav > div a.nav-item.nav-link.active
{
border: none;
  padding: 18px 25px;
  color:#fff;
  background:#272e38;
  border-radius:10;
}

nav > div a.nav-item.nav-link.active:after
{
content: "";
position: relative;
bottom: -60px;
left: -10%;
border: 15px solid transparent;
border-top-color: #0026F5;
}
.tab-content{
background: #0026F5;
  line-height: 25px;
  border: 1px solid #ddd;
  border-top:5px solid #e74c3c;
  border-bottom:5px solid #e74c3c;
  padding:30px 25px;
}
.card-header:hover{
    background: #e6e6e6;
    cursor: pointer;

}

.btn {
    width:100%; 
    text-align: left;
}

.btn-link{
    color: black;
}

.btn-link:hover{
    color: #0026F5;
}

#collapsible:after {
  content: '\002B';
  color: white;
  font-weight: bold;
  float: right;
  margin-left: 5px;
}

.active:after {
  content: "\2212";
}

h5{
    color: black;
}

a {
    color: black;
}
a:hover {
    color: #0026F5;
    text-decoration-line: none;
}

</style>

    <div class="col-lg-8 bg-light">
        <div class="mb-4">
            <nav>
            <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                <a href="/current-issues" class="nav-item nav-link" id="nav-home-tab" role="tab" aria-controls="nav-home" aria-selected="true"><i class="fa fa-book" aria-hidden="true" style="width: 10px;"></i><span> &nbsp;</span> current issue</a>
                <a href="/browse-issues" style="background-color: #0026F5;" class="nav-item nav-link active" id="nav-profile-tab" role="tab" aria-controls="nav-profile" aria-selected="false"><i class="fa fa-list" aria-hidden="true"></i><span> &nbsp;</span> browse issue</a>
            </div>
            </nav>
        </div>
        <div class="mb-5">
            <h5>List Issues:</h5>
            <div class="accordion" id="accordionExample">
                
                <div class="card">
                    <div class="card-header" id="headingNine">
                    <h5 class="mb-0">
                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
                        Volume 1 2011
                        </button>
                    </h5>
                    </div>
                    <div id="collapseNine" class="collapse" aria-labelledby="headingNine" data-parent="#accordionExample">
                        <div class="card pl-4">
                            <a href="/issue-2-number-1" ><strong>&nbsp; issue 2</strong></a>
                            <small>&nbsp; 2020 pages 757-869</small>
                        </div>
                        <div class="card pl-4">
                            <a href="/issue-1-number-2" ><strong>&nbsp; issue 1 number 2</strong></a>
                            <small>&nbsp; 2020 pages 757-869</small>
                        </div>
                        <div class="card pl-4">
                            <a href="/issue-1-number-1" ><strong>&nbsp; issue 1 number 1</strong></a>
                            <small>&nbsp; 2020 pages 757-869</small>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection