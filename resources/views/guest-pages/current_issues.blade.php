@extends('app')

@section('content')

<style>
nav > .nav.nav-tabs{

border: none;
  color:#fff;
  background:#272e38;
  border-radius:0;

}
nav > div a.nav-item.nav-link,
nav > div a.nav-item.nav-link.active
{
border: none;
  padding: 18px 25px;
  color:#fff;
  background:#272e38;
  border-radius:10;
}

nav > div a.nav-item.nav-link.active:after
{
content: "";
position: relative;
bottom: -60px;
left: -10%;
border: 15px solid transparent;
border-top-color: #0026F5;
}
.tab-content{
background: #0026F5;
  line-height: 25px;
  border: 1px solid #ddd;
  border-top:5px solid #e74c3c;
  border-bottom:5px solid #e74c3c;
  padding:30px 25px;
}

</style>

  <div class="col-lg-8 bg-light">
    <div class="mb-4">
      <nav>
        <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
          <a href="/current-issues" style="background-color: #0026F5;" class="nav-item nav-link active" id="nav-home-tab" role="tab" aria-controls="nav-home" aria-selected="true"><i class="fa fa-book" aria-hidden="true" style="width: 10px;"></i><span> &nbsp;</span> current issue</a>
          <a href="/browse-issues" class="nav-item nav-link" id="nav-profile-tab" role="tab" aria-controls="nav-profile" aria-selected="false"><i class="fa fa-list" aria-hidden="true"></i><span> &nbsp;</span> browse issue</a>
        </div>
      </nav>
    </div>
    <div class="mb-3">
          <div class="card w-100">
              <div class="card-body">
                  <h5 class="card-title">Using two-eyed seeing to bridge Western science and Indigenous knowledge systems and understand long-term change in the Saskatchewan River Delta, Canada</h5>
                  <p>Razak Abu , Maureen G. Reed & Timothy D. Jardine</p>
                  <p><strong> Published online: </strong>21 Jan 2019</p>
                  <a href="/images/myw3schoolsimage.jpg" download>Download Article as PDF</a>
              </div>
          </div>
    </div>

    <div class="mb-3">
          <div class="card w-100">
              <div class="card-body">
                  <h5 class="card-title">Using two-eyed seeing to bridge Western science and Indigenous knowledge systems and understand long-term change in the Saskatchewan River Delta, Canada</h5>
                  <p>Razak Abu , Maureen G. Reed & Timothy D. Jardine</p>
                  <p><strong> Published online: </strong>21 Jan 2019</p>
                  <a href="/images/myw3schoolsimage.jpg" download>Download Article as PDF</a>
              </div>
          </div>
    </div>

    <div class="mb-3">
          <div class="card w-100">
              <div class="card-body">
                  <h5 class="card-title">Using two-eyed seeing to bridge Western science and Indigenous knowledge systems and understand long-term change in the Saskatchewan River Delta, Canada</h5>
                  <p>Razak Abu , Maureen G. Reed & Timothy D. Jardine</p>
                  <p><strong> Published online: </strong>21 Jan 2019</p>
                  <a href="/images/myw3schoolsimage.jpg" download>Download Article as PDF</a>
              </div>
          </div>
    </div>

    <div class="mb-3">
          <div class="card w-100">
              <div class="card-body">
                  <h5 class="card-title">Using two-eyed seeing to bridge Western science and Indigenous knowledge systems and understand long-term change in the Saskatchewan River Delta, Canada</h5>
                  <p>Razak Abu , Maureen G. Reed & Timothy D. Jardine</p>
                  <p><strong> Published online: </strong>21 Jan 2019</p>
                  <a href="/images/myw3schoolsimage.jpg" download>Download Article as PDF</a>
              </div>
          </div>
    </div>

    <div class="mb-3">
          <div class="card w-100">
              <div class="card-body">
                  <h5 class="card-title">Using two-eyed seeing to bridge Western science and Indigenous knowledge systems and understand long-term change in the Saskatchewan River Delta, Canada</h5>
                  <p>Razak Abu , Maureen G. Reed & Timothy D. Jardine</p>
                  <p><strong> Published online: </strong>21 Jan 2019</p>
                  <a href="/images/myw3schoolsimage.jpg" download>Download Article as PDF</a>
              </div>
          </div>
    </div>

    <div class="mb-3">
          <div class="card w-100">
              <div class="card-body">
                  <h5 class="card-title">Using two-eyed seeing to bridge Western science and Indigenous knowledge systems and understand long-term change in the Saskatchewan River Delta, Canada</h5>
                  <p>Razak Abu , Maureen G. Reed & Timothy D. Jardine</p>
                  <p><strong> Published online: </strong>21 Jan 2019</p>
                  <a href="/images/myw3schoolsimage.jpg" download>Download Article as PDF</a>
              </div>
          </div>
    </div>

    <div class="mb-3">
          <div class="card w-100">
              <div class="card-body">
                  <h5 class="card-title">Using two-eyed seeing to bridge Western science and Indigenous knowledge systems and understand long-term change in the Saskatchewan River Delta, Canada</h5>
                  <p>Razak Abu , Maureen G. Reed & Timothy D. Jardine</p>
                  <p><strong> Published online: </strong>21 Jan 2019</p>
                  <a href="/images/myw3schoolsimage.jpg" download>Download Article as PDF</a>
              </div>
          </div>
    </div>

    <div class="mb-3">
          <div class="card w-100">
              <div class="card-body">
                  <h5 class="card-title">Using two-eyed seeing to bridge Western science and Indigenous knowledge systems and understand long-term change in the Saskatchewan River Delta, Canada</h5>
                  <p>Razak Abu , Maureen G. Reed & Timothy D. Jardine</p>
                  <p><strong> Published online: </strong>21 Jan 2019</p>
                  <a href="/images/myw3schoolsimage.jpg" download>Download Article as PDF</a>
              </div>
          </div>
    </div>
  </div>

@endsection
