
@extends('app')

@section('content')

  <div class="col-lg-8 bg-light">

      <div class="mb-5">
        <h3 class="h5 d-flex align-items-center mb-4 text-info"><span class="icon-align-left mr-3"></span>Editorial Board Members </h3>
      </div>

      <div class="mb-5">
        <h3 class="h5 d-flex align-items-center mb-4 " style="color: #0026F5;"><span class="icon-people mr-3"></span><b>Dr. Chealy Chet</b>, Nagoya University, Japan </h3>
        <ul class=" ">
          <li class="d-flex align-items-start mb-2"><span>Rector of the Royal University of Phnom Penh, Cambodia</span></li>
          <li class="d-flex align-items-start mb-2"><span>Higher education in Cambodia</span></li>
        </ul>
      </div>

      <div class="mb-5">
        <h3 class="h5 d-flex align-items-center mb-4 " style="color: #0026F5;"><span class="icon-people mr-3"></span><b>Dr. Vanny Sok</b>, Distance Learning Institutes, Indonesia</h3>
        <ul class=" ">
          <li class="d-flex align-items-start mb-2"><span>Vice-rector, Royal University of Phnom Penh</span></li>
          <li class="d-flex align-items-start mb-2"><span>Business and Management </span></li>
        </ul>
      </div>

      <div class="mb-5">
        <h3 class="h5 d-flex align-items-center mb-4 " style="color: #0026F5;"><span class="icon-people mr-3"></span><b>Dr. George Chigas</b>, University of London SOAS, United Kingdom</h3>
        <ul class=" ">
          <li class="d-flex align-items-start mb-2"><span>Associate Teaching Professor at University of Massachusetts Lowell, USA</span></li>
          <li class="d-flex align-items-start mb-2"><span>Cambodian Literature & Culture, Genocide & Survivor Literature</span></li>
        </ul>
      </div>

      <div class="mb-5">
        <h3 class="h5 d-flex align-items-center mb-4 " style="color: #0026F5;"><span class="icon-people mr-3"></span><b>Dr. Sanara Hor</b>, Kyoto University, Japan</h3>
        <ul class=" ">
          <li class="d-flex align-items-start mb-2"><span>Dean of Faculty of Land Management and Land Administration</span></li>
          <li class="d-flex align-items-start mb-2"><span>Royal University of Agriculture, Cambodia </span></li>
          <li class="d-flex align-items-start mb-2"><span>Land management and natural resource management </span></li>
        </ul>
      </div>

      <div class="mb-5">
        <h3 class="h5 d-flex align-items-center mb-4 " style="color: #0026F5;"><span class="icon-people mr-3"></span><b>Dr. Seak Sophat</b>, Asian Institute of Technology, Thailand </h3>
        <ul class=" ">
          <li class="d-flex align-items-start mb-2"><span>Vice-dean of Faculty of Development Studies</span></li>
          <li class="d-flex align-items-start mb-2"><span>Urbanization, climate change and natural resource management</span></li>
        </ul>
      </div>

      <div class="mb-5">
        <h3 class="h5 d-flex align-items-center mb-4 " style="color: #0026F5;"><span class="icon-people mr-3"></span><b>Dr. Neak Chandarith</b>, Australian National University, Australia </h3>
        <ul class=" ">
          <li class="d-flex align-items-start mb-2"><span>Head of Department of International Studies (DIS)</span></li>
          <li class="d-flex align-items-start mb-2"><span>Royal University of Phnom Penh (RUPP)</span></li>
          <li class="d-flex align-items-start mb-2"><i>Section Editor-in-Chief</i></li>
          <li class="d-flex align-items-start mb-2"><p>International development and political science </p></li>
        </ul>
      </div>

      <div class="mb-5">
        <h3 class="h5 d-flex align-items-center mb-4 " style="color: #0026F5;"><span class="icon-people mr-3"></span><b>Dr. Danny Marks</b>, University of Sydney</h3>
        <ul class=" ">
          <li class="d-flex align-items-start mb-2"><span>Assistant Professor at Dublin City University</span></li>
          <li class="d-flex align-items-start mb-2"><span>Environmental governance and politics, climate change policy and disaster governance</span></li>
        </ul>
      </div>

      <div class="mb-5">
        <h3 class="h5 d-flex align-items-center mb-4 " style="color: #0026F5;"><span class="icon-people mr-3"></span><b>Dr. Rath Sethik</b>, Lucian Blaga University of Sibiu, Romania</h3>
        <ul class=" ">
          <li class="d-flex align-items-start mb-2"><span>Dean of Faculty of Development Studies </span></li>
          <li class="d-flex align-items-start mb-2"><span>Royal University of Phnom Penh</span></li>
          <li class="d-flex align-items-start mb-2"><span>Forest policy, rural development and natural resource management </span></li>
        </ul>
      </div>

      <div class="mb-5">
        <h3 class="h5 d-flex align-items-center mb-4 " style="color: #0026F5;"><span class="icon-people mr-3"></span><b>Dr. Oeurng Chantha</b>, University of Toulouse France</h3>
        <ul class=" ">
          <li class="d-flex align-items-start mb-2"><span>Vice-rector of Institute of Technology of Cambodia, Cambodia</span></li>
          <li class="d-flex align-items-start mb-2"><span>Hydrology and Water Resources Engineering</span></li>
        </ul>
      </div>

      <div class="mb-5">
        <h3 class="h5 d-flex align-items-center mb-4 " style="color: #0026F5;"><span class="icon-people mr-3"></span><b>Dr. Say Sok</b>, Deakin University, Australia </h3>
        <ul class=" ">
          <li class="d-flex align-items-start mb-2"><span>World Bank consultant, Cambodia</span></li>
          <li class="d-flex align-items-start mb-2"><span>Political Science, political economy and resources governance </span></li>
        </ul>
      </div>

      <div class="mb-5">
        <h3 class="h5 d-flex align-items-center mb-4 " style="color: #0026F5;"><span class="icon-people mr-3"></span><b>Dr. Jing Liu</b>, Nagoya University, Japan</h3>
        <ul class=" ">
          <li class="d-flex align-items-start mb-2"><span>Associate Professor at Tohoku University, Japan</span></li>
          <li class="d-flex align-items-start mb-2"><span>Public education, human resource development and inequality in education</span></li>
        </ul>
      </div>

      <div class="mb-5">
        <h3 class="h5 d-flex align-items-center mb-4 " style="color: #0026F5;"><span class="icon-people mr-3"></span><b>Dr. Blessing Gweshengwe</b>, Universiti Brunei Darussalam, Brunei</h3>
        <ul class=" ">
          <li class="d-flex align-items-start mb-2"><span>Lecturer, Great Zimbabwe University, Zimbabwe  </span></li>
          <li class="d-flex align-items-start mb-2"><span>Rural and urban poverty, income inequality and Seasonal poverty</span></li>
        </ul>
      </div>

      <div class="mb-5">
        <h3 class="h5 d-flex align-items-center mb-4 " style="color: #0026F5;"><span class="icon-people mr-3"></span><b>Dr. Kimly Ngoun</b>, Australian National University, Australia </h3>
        <ul class=" ">
          <li class="d-flex align-items-start mb-2"><span>Director of Research and Editor at Asian Vision Institute, Cambodia </span></li>
          <li class="d-flex align-items-start mb-2"><span>Political and social change. </span></li>
        </ul>
      </div>

      <div class="mb-5">
        <h3 class="h5 d-flex align-items-center mb-4  text-info"><span class="icon-align-left mr-3"></span>Editorial Office</h3>
      </div>

      <div class="mb-5">
        <h3 class="h5 d-flex align-items-center mb-4 " style="color: #0026F5;"><span class="icon-people mr-3"></span><b>Mr. Reno THOU</b>, Manager</h3>
        <ul class=" ">
          <li class="d-flex align-items-start mb-2"><span>Head of Research Office</span></li>
          <li class="d-flex align-items-start mb-2"><span>Royal University of Phnom Penh</span></li>
        </ul>
      </div>

      <div class="mb-5">
        <h3 class="h5 d-flex align-items-center mb-4 " style="color: #0026F5;"><span class="icon-people mr-3"></span><b>Ms. Soviphea CHENDA</b>, Assistant </h3>
        <ul class=" ">
          <li class="d-flex align-items-start mb-2"><span>Researcher of Research Office</span></li>
          <li class="d-flex align-items-start mb-2"><span>Royal University of Phnom Penh</span></li>
        </ul>
      </div>

  </div>

@endsection
