
<style>
nav > .nav.nav-tabs{

border: none;
  color:#fff;
  background:#272e38;
  border-radius:0;

}
nav > div a.nav-item.nav-link,
nav > div a.nav-item.nav-link.active
{
border: none;
  padding: 18px 25px;
  color:#fff;
  background:#272e38;
  border-radius:10;
}

nav > div a.nav-item.nav-link.active:after
{
content: "";
position: relative;
bottom: -60px;
left: -10%;
border: 15px solid transparent;
border-top-color: #0026F5;
}
.tab-content{
background: #0026F5;
  line-height: 25px;
  border: 1px solid #ddd;
  border-top:5px solid #e74c3c;
  border-bottom:5px solid #e74c3c;
  padding:30px 25px;
}

nav > div a.nav-item.nav-link:hover,
nav > div a.nav-item.nav-link:focus
{
border: none;
  background: #0026F5;
  color:#fff;
  border-radius:0;
  transition:background 0.20s linear;
}
</style>
@extends('app')

@section('content')
  <div class="col-lg-8 bg-light">
    <div class="mb-5">
      <nav>
        <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
          <a href="/current-issues" class="nav-item nav-link active" id="nav-home-tab" role="tab" aria-controls="nav-home" aria-selected="true"><i class="fa fa-book" aria-hidden="true" style="width: 10px;"></i><span> &nbsp;</span> current issue</a>
          <a href="/browse-issues" class="nav-item nav-link" id="nav-profile-tab" role="tab" aria-controls="nav-profile" aria-selected="false"><i class="fa fa-list" aria-hidden="true"></i><span> &nbsp;</span> browse issue</a>
        </div>
      </nav>
    </div>
    <div class="mb-5">
      <h3 class="h5 d-flex align-items-center mb-4 text-info"><span class="icon-align-left mr-3"></span>Aim and Scope</h3>
      <p class="text-justify">Insight: Cambodia Journal of Basic and Applied Research (CJBAR) is an academic, policy and practice-oriented journal, which covers all aspects of science
        and engineering, the social sciences and humanities, education, development studies, and languages. Academic or applied research manuscripts from within Cambodia;
        or from outside Cambodia but contributing to the social, economic, or environmental development of Cambodia, ASEAN, or the Greater Mekong Subregion may be submitted to the journal.
        The journal welcomes manuscripts from any discipline, where theories, concepts, innovations, new technologies,
        or best practices are introduced. However, the journal reserves the right to prioritize research focused on topics aligned with the courses offered at RUPP. </p>
      <p class="text-justify">This is a bilingual Journal where an author many decide to publish his/her manuscript either in English or
      Khmer. However, all manuscripts published in the CJBAR must have abstracts in both English and Khmer. </p>
      <p>The Journal publishes two issues per annum:</p>
      <ul>
        <li><b>Issue 1:</b> January-June</li>
        <li><b>Issue 2:</b> July- December</li>
      <ul>

    </div>
  </div>
@endsection
