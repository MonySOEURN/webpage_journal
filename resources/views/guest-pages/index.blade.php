@extends('app')

@section('content')
    <div class="col-lg-8 bg-light">
        <div class="mb-5">
            <figure class="mb-5"></figure>
            <h3 class="h5 d-flex align-items-center mb-4 text-info"><span class="icon-align-left mr-3"></span>About CJBAR</h3>
            <p class="text-justify">
                Insight: The Cambodia Journal of Basic and Applied Research (CJBAR) has been approved by the Ministry of Information (MoI) upon the Royal University of Phnom Penh or RUPP’s request, No. 770/2018 RUPP, dated
                on 16 August 2018. The preparation and printing of the Journal is generously supported by the Research Office of the RUPP, who also provided technical support in hosting, editing, and publishing the CJBAR.
                The RUPP is the oldest and the largest public university in Cambodia. It has contributed significantly to development of human resources for many sectors, especially to the training of teachers in high
                schools and other public servants. With respect to Cambodia’s integration into the ASEAN Economic Community in 2015, the role of RUPP in furthering the scope of these achievements will need to be improved and
                strengthened accordingly. A situation analysis has shown that RUPP faces with considerable challenges in the further development of capacity in Cambodia, requiring comprehensive reform in terms of leadership
                and management, administration, and financing of the university. Our goal is to position the right people in the right place, improving teaching and learning methods, enhancing institutional capacity for
                research and development, and developing infrastructure and campus services. We believe this is essential for transforming RUPP into the flagship public university in Cambodia. Currently, 6% of our full-time
                staff hold doctoral degrees, 64% have master’s degrees and 93% of our administration and finance staff are qualified to at least a bachelor degree level. We have almost 20,000 students and have more students
                receiving support from scholarships and fee waivers than any other large publicly funded university in Cambodia.
            </p>
        </div>
    </div>
@endsection
            
