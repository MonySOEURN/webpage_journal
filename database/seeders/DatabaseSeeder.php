<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            UserSeeder::class,
            SliderSeeder::class,
            MenuSeeder::class,
            PaperSeeder::class,
            JournalSeeder::class,
            JournalDetailSeeder::class,
            MenuDetailSeeder::class,
            ArticleSeeder::class,
        ]);
    }
}
