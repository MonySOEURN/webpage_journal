<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

use Faker\Factory as Faker;

class PaperSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach (range(1,10) as $index) {
            DB::table('papers')->insert(
                [
                    'title' => $faker->sentence,
                    'author' => $faker->title.' '.$faker->userName,
                    'page_no' => rand(20, 70),
                    'created_at' => Carbon::now(),
                ]
            );
        }
    }
}
