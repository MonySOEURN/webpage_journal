<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

use Faker\Factory as Faker;

class JournalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach (range(1,3) as $index) {
            DB::table('journals')->insert(
                [
                    'user_id' => rand(1,3),
                    'volume_no' => $faker->postCode,
                    'created_at' => Carbon::now(),
                ]
            );
        }
    }
}
