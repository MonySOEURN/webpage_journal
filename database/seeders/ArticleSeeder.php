<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ArticleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('articles')->insert(array (
            
            array( 
                'name' => 'Paramita (Guha) Ghosh', 
                'title' => 'Deep Learning Updates: Machine Learning, Deep Reinforcement Learning, and Limitations', 
                'date' => 'October 1, 2020'),
            array( 'name' => 'Amber Lee Dennis',
                'title' => 'Data Governance: Balancing Security and Availability', 
                'date' => 'September 30, 2020'),
            
            array( 'name' => 'Paramita (Guha) Ghosh',
                'title' => 'Data Architecture and Artificial Intelligence: How Do They Work Together?', 
                'date' => 'September 29, 2020'),
            array( 'name' => 'Jennifer Zaino',
                'title' => 'Case Study: Data Protection and HCI for Carrs Tool Data Architecture Upgrade', 
                'date' => 'September 24, 2020'),
            array( 'name' => 'Michelle Knight',
                'title' => 'NoSQL Databases: The Versatile Solution for Continuous Intelligence', 
                'date' => 'September 22, 2020'),
            array( 'name' => 'Jennifer Zaino',
                'title' => 'Data Quality’s Importance to Blockchain', 
                'date' => 'September 17, 2020'),
            array( 'name' => 'Keith D. Foote',
                'title' => 'A Brief History of Data Containers', 
                'date' => 'September 15, 2020'),
            array( 'name' => 'Michelle Knight',
                'title' => 'Enterprise Data Literacy: Understanding Data Management', 
                'date' => 'September 8, 2020'),
            array( 'name' => 'Paramita (Guha) Ghosh',
                'title' => 'Data Governance and Serverless Computing', 
                'date' => 'September 3, 2020'),
            array( 'name' => 'Paramita (Guha) Ghosh',
                'title' => 'Data Fabric Use Cases', 
                'date' => 'September 1, 2020'),
            array( 'name' => 'Amber Lee Dennis',
                'title' => 'Metadata Repository Essential Use Cases', 
                'date' => 'August 27, 2020'),
            array( 'name' => 'Michelle Knight',
                'title' => 'Unify Data Governance with Data Architecture', 
                'date' => 'August 25, 2020'),
            array( 'name' => 'Keith D. Foote',
                'title' => 'A Brief History of Open Source Data Technologies', 
                'date' => 'August 20, 2020'),
            array( 'name' => 'Paramita (Guha) Ghosh',
                'title' => 'Fundamentals of Machine Learning Enabled Analytics', 
                'date' => 'August 18, 2020'),
            array( 'name' => 'Keith D. Foote',
                'title' => 'So You Want to Be a Data Modeler?', 
                'date' => 'August 11, 2020'),
            array( 'name' => 'Paramita (Guha) Ghosh',
                'title' => 'How Can Data Strategy Strengthen Advanced Analytics?', 
                'date' => 'August 6, 2020'),
            array( 'name' => 'Michelle Knight',
                'title' => 'Case Study: AK Productions Increases Efficiency with Cloud Storage Solution', 
                'date' => 'August 4, 2020'),
            array( 'name' => 'Michelle Knight',
                'title' => 'Metadata Repository Basics: From Database to Data Architecture', 
                'date' => 'July 29, 2020'),
            array( 'name' => 'Paramita (Guha) Ghosh',
                'title' => 'Evaluating Enterprise Data Literacy', 
                'date' => 'July 28, 2020'),
            array( 'name' => 'Michelle Knight',
                'title' => 'Data Governance and Data Stewardship Drive Successful Glossaries and Dictionaries', 
                'date' => 'July 23, 2020'),
            array( 'name' => 'Keith D. Foote',
                'title' => 'So You Want to be a Data Governance Specialist?', 
                'date' => 'July 22, 2020'),
            array( 'name' => 'Paramita (Guha) Ghosh',
                'title' => 'How Does Data Management Drive Efficiency for Organizations?', 
                'date' => 'July 21, 2020'),
            array( 'name' => 'Michelle Knight',
                'title' => 'Databases vs. Hadoop vs. Cloud Storage', 
                'date' => 'July 15, 2020'),
        ));
    }
}
