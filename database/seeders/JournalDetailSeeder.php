<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

use Faker\Factory as Faker;

class JournalDetailSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach (range(1,3) as $index) {
            DB::table('journal_details')->insert(
                [
                    'journal_id' => rand(1, 3),
                    'paper_id' => rand(1, 10),
                    'name' => $faker->userName,
                    'year' => rand(1999,2020),
                    'created_at' => Carbon::now(),
                ]
            );
        }
    }
}
