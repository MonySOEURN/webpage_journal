<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

use Faker\Factory as Faker;

class SliderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach (range(1,10) as $index) {
            DB::table('sliders')->insert(
                [
                    'image' => 'https://picsum.photos/id/'.rand(1, 1000).'/940/352',
                    'quotation' => $faker->name(),
                    'created_at' => Carbon::now(),
                ]
            );
        }
    }
}
