<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

use Faker\Factory as Faker;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach (range(1,3) as $index) {
            DB::table('users')->insert(
                [
                    'name' => $faker->userName,
                    'email' => $faker->email,
                    'password' => bcrypt('12345678'),
                    'created_at' => Carbon::now(),
                ]
            );
        }
    }
}
