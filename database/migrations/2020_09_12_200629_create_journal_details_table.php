<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJournalDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('journal_details', function (Blueprint $table) {
            $table->id();    
            $table->unsignedBigInteger('journal_id');
            $table->unsignedBigInteger('paper_id');
            $table->string('name');
            $table->string('year');

            $table->foreign('journal_id')->references('id')->on('journals');
            $table->foreign('paper_id')->references('id')->on('papers');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('journal_details');
    }
}
