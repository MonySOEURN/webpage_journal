<?php

namespace App\Http\Controllers;

use App\Models\Article;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SearchJournalController extends Controller
{

    public function getResult(Request $request){
        $author = $request->input('author');
        $title = $request->input('title');
        if($author == null){
            $results = Article::query()
            ->where('title', 'LIKE', "%{$title}%") 
            ->paginate(5);
        } 
        if ($title == null){
            $results = Article::query()
            ->where('name', 'LIKE', "%{$author}%") 
            ->paginate(5);
        } 
        if ( ($title && $author) != null ) {
            $results = Article::query()
            ->where('name', 'LIKE', "%{$author}%") 
            ->orWhere('title', 'LIKE', "%{$title}%") 
            ->paginate(5);
        }
        $total = $results->total();
        return view('guest-pages.search_result')
                ->with('results', $results)
                ->with('total', $total);
    }
}
