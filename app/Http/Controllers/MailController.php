<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Barryvdh\DomPDF\Facade as PDF;
// use PDF
 
class MailController extends Controller
{
    public function sendMail(Request $request)
    {

        $data = ['data' => 'name2'];


        $pdf = PDF::loadView('guest-pages.submitedFormpdf', $data);
        // return $pdf->download('document.pdf');

        Mail::send('guest-pages.submitedFormpdf', $data, function($message) use ($pdf){
            $message->from('info@test.com*');
            $message->to('mail@codechief.org');
            $message->subject('Date wise user report');
            $message->attachData($pdf->output(),'document.pdf');
        });

    //    $pdf->SetProtection(['copy', 'print'], '', 'pass');
       return $pdf->stream('document.pdf');

    }
}
